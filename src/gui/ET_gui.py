import sys
import os
import tkinter as tk
import tkinter.messagebox as tkm
from PIL import Image, ImageTk
import camera.ET_Camera as ET_Camera

import time


class GUI:
    def __init__(self):    # コンストラクタ
        print("GUI initialize")

        # GUI生成
        # root
        self.root = tk.Tk()
        self.ROOT_X = 1280          # GUIサイズ
        self.ROOT_Y = 700
        self.LF_CAM_X = 704+10      # カメラ画像表示用フレームサイズ
        self.LF_CAM_Y = 460
        self.LF_SETTING_X = 480        # 設定用フレームサイズ
        self.LF_SETTING_Y = 100
        self.LF_IP_X = self.LF_CAM_X     # 画像処理結果表示用フレームサイズ
        self.LF_IP_Y = 200
        self.LF_RECO_X = self.LF_SETTING_X     # 認識結果表示用フレームサイズ
        self.LF_RECO_Y = 355

        self.CAM_DISP_SIZE_X = 704
        self.CAM_DISP_SIZE_Y = 396
        self.CAM_CANVAS_X = self.CAM_DISP_SIZE_X    # カメラ画像サイズ
        self.CAM_CANVAS_Y = self.CAM_DISP_SIZE_Y
        self.RECO_CANVAS_X = self.LF_RECO_X-20      # 認識結果キャンバスサイズ
        self.RECO_CANVAS_Y = self.LF_RECO_Y-30
        self.RECO_DISP_X = self.RECO_CANVAS_X-5     # 認識結果フィールド画像サイズ
        self.RECO_DISP_Y = self.RECO_CANVAS_Y-5

        self.root.title(u"cvgui test")
        self.root.geometry(str(self.ROOT_X) + "x" + str(self.ROOT_Y))
        self.root.resizable(width=0, height=0)

        self.root.protocol("WM_DELETE_WINDOW", self.on_closing)  # 終了時コールバック

        # 4隅初期配置
        self.bb_points_info = [{'x': 50.0, 'y': 50.0, 'color': 'red', 'tag_c': "bb_circle_0", 'tag_l': "bb_line_0", "id_c": 0, "id_l": 0},
                               {'x': 100.0, 'y': 50.0, 'color': 'blue', 'tag_c': "bb_circle_1", 'tag_l': "bb_line_1", "id_c": 0, "id_l": 0},
                               {'x': 100.0, 'y': 100.0, 'color': 'blue', 'tag_c': "bb_circle_2", 'tag_l': "bb_line_2", "id_c": 0, "id_l": 0},
                               {'x': 50.0, 'y': 100.0, 'color': 'blue', 'tag_c': "bb_circle_3", 'tag_l': "bb_line_3", "id_c": 0, "id_l": 0}]

        self.cam_connected = False      # カメラ接続済み
        self.__ReqRecognition = False   # 認識実行要求

        self.__firstFrame()
        self.__afterMSec()

    def on_closing(self):
        if tkm.askokcancel("終了確認", "終了します"):
            print("GUI close")
            if self.cam_connected:
                self.camera.closeCamera()
            self.root.destroy()

    def __firstFrame(self):       # 初めに一回
        self.first_frame = tk.Frame(self.root, bd=2, relief="groove",
                                    width=self.ROOT_X, height=self.ROOT_Y)
        self.first_frame.grid(row=0, column=0)

        # カメラ画像表示エリア
        self.lf_cam = tk.LabelFrame(self.first_frame, text="カメラ画像", width=self.LF_CAM_X, height=self.LF_CAM_Y)
        self.lf_cam.propagate(False)
        self.lf_cam.place(x=10, y=10)

        # 設定表示エリア
        self.lf_setting = tk.LabelFrame(self.first_frame, text="設定", width=self.LF_SETTING_X, height=self.LF_SETTING_Y)
        self.lf_setting.propagate(False)
        self.lf_setting.place(x=10 + self.LF_CAM_X + 5, y=10)

        # 認識結果表示エリア
        self.lf_reco = tk.LabelFrame(self.first_frame, text="認識結果", width=self.LF_RECO_X, height=self.LF_RECO_Y)
        self.lf_reco.propagate(False)
        self.lf_reco.place(x=10 + self.LF_CAM_X + 5, y=10 + self.LF_SETTING_Y + 5)

        # 画像処理表示エリア
        self.lf_ip = tk.LabelFrame(self.first_frame, text="画像処理", width=self.LF_IP_X, height=self.LF_IP_Y)
        self.lf_ip.propagate(False)
        self.lf_ip.place(x=10, y=10 + self.LF_CAM_Y + 5)



        # カメラ画像表示キャンバス
        self.cam_canvas = tk.Canvas(self.lf_cam, width=self.CAM_CANVAS_X, height=self.CAM_CANVAS_Y, relief="flat")
        self.cam_canvas.place(x=2, y=2)
        self.camimgrectid = self.cam_canvas.create_rectangle(0, 0, self.CAM_CANVAS_X, self.CAM_CANVAS_Y, fill="#696969")
        self.camimgid = 0

        # fps表示
        self.cam_canvas_fps_label = self.cam_canvas.create_text(30, 8, text="0.0fps", fill="white")

        # 処理時間計測
        self.starttime = time.time()

        # ボタン
        # ラズパイカメラオープン
        self.button_open_raspi_camera = tk.Button(self.lf_cam, text=u'ラズパイカメラオープン', width=15, height=1, command=self.__button_open_raspi_camera_clicked, relief="raised")
        #self.button_open_raspi_camera.bind("<Button-1>", self.button_open_raspi_camera_clicked)    # bindだと無効化時も押せてしまう
        self.button_open_raspi_camera.place(x=10, y=10+self.CAM_CANVAS_Y)
        # PCカメラオープン
        self.button_open_pc_camera = tk.Button(self.lf_cam, text=u'PCカメラオープン', width=15, height=1, command=self.__button_open_pc_camera_clicked, relief="raised")
        self.button_open_pc_camera.place(x=130, y=10+self.CAM_CANVAS_Y)

        # 認識結果キャンバス
        self.reco_canvas = tk.Canvas(self.lf_reco, width=self.RECO_CANVAS_X, height=self.RECO_CANVAS_Y, relief="flat")
        self.reco_canvas.place(x=5, y=2)
        # フィールド画像
        courseimg_image = Image.open(os.path.dirname(__file__) + "/img/" + "course_l.png")
        self.courseimg_image_r = courseimg_image.resize((self.RECO_DISP_X, self.RECO_DISP_Y), Image.BICUBIC)
        self.courseimg = ImageTk.PhotoImage(self.courseimg_image_r)
        self.reco_field_img_id = self.reco_canvas.create_image(self.RECO_CANVAS_X / 2, self.RECO_CANVAS_Y / 2, image=self.courseimg)

    def __afterMSec(self):    # 周期処理

        self.__updateCamCanvas()      # カメラ画像更新

        ReqTransmit = False
        # 【走行開始による認識実行】
        # GUIのゲーム開始ボタン押されている (←不要？ 押し忘れの可能性あり)
        if self.__judgeStartGame():              # 走行開始判定
            if True: #前回は開始していなかった
                self.__ReqRecognition = True    # 認識実行要求
                ReqTransmit = True            # 走行体へ送信要求 有効


        # 認識要求有り (ゲーム開始による判定 もしくは，GUIのボタンによりフラグが立つ)
        if self.__ReqRecognition:
            self.__ReqRecognition = False
            # 認識実行
            # reco_result = self.recognition(Course, Img, bb_area_points, number_card_points)
            # reco_resultには，数字，ブロックの位置，色情報が入っている
            # 認識結果表示
            # self.setRecoResult(reco_result)
            # if ReqTransmit:
            #   走行体へ通信

        self.root.after(33, self.__afterMSec)

    def __updateCamCanvas(self):
        fps_f = 0.0
        # 画像更新
        if self.cam_connected:
            self.cam_img_raw = Image.fromarray(self.camera.frame)
            self.cam_img_raw_r = self.cam_img_raw.resize((self.CAM_DISP_SIZE_X, self.CAM_DISP_SIZE_Y), Image.BICUBIC)
            self.canvas_img = ImageTk.PhotoImage(self.cam_img_raw_r)
            if self.camimgid == 0:
                self.camimgid = self.cam_canvas.create_image(self.CAM_CANVAS_X / 2, self.CAM_CANVAS_Y / 2, image=self.canvas_img)
            else:
                self.cam_canvas.itemconfigure(tagOrId=self.camimgid, image=self.canvas_img)

            self.lasttime = self.starttime
            self.starttime = time.time()
            if (self.starttime - self.lasttime) > 0:
                fps_f = 1 / (self.starttime - self.lasttime)
        else:   # カメラ未接続
            # カメラ画像削除
            if not self.camimgid == 0:
                self.cam_canvas.delete(self.camimgid)
                self.camimgid = 0

        # 点表示更新
        self.__updateSelPoint4()

        # フレームレート表示更新
        self.cam_canvas.itemconfigure(tagOrId=self.cam_canvas_fps_label, text=f"{fps_f:02.2f}" + " fps")
        self.cam_canvas.lift(self.cam_canvas_fps_label)

    def __updateSelPoint4(self):
        for idx, selpoint in enumerate(self.bb_points_info):
            if idx < len(self.bb_points_info)-1:  # 最後でない
                nextpoint_idx = idx+1
            else:
                nextpoint_idx = 0

            # ライン描画
            if not selpoint["tag_l"] in self.cam_canvas.itemcget(self.bb_points_info[idx]["id_l"], 'tags'):     # 無ければ
                self.bb_points_info[idx]["id_l"] = self.cam_canvas.create_line(selpoint["x"], selpoint["y"], self.bb_points_info[nextpoint_idx]["x"], self.bb_points_info[nextpoint_idx]["y"], width=2.0, fill='green', tag=selpoint["tag_l"])

            # ○描画
            if not selpoint["tag_c"] in self.cam_canvas.itemcget(self.bb_points_info[idx]["id_c"], 'tags'):     # 無ければ
                self.bb_points_info[idx]["id_c"] = self.cam_canvas.create_oval(selpoint["x"] - 5, selpoint["y"] - 5, selpoint["x"] + 5, selpoint["y"] + 5, fill=self.bb_points_info[idx]["color"],
                                                                               tag=selpoint["tag_c"])  # 円で塗りつぶし
                self.cam_canvas.tag_bind(self.bb_points_info[idx]["id_c"], "<B1-Motion>", self.__bb_circle_dragged)   # ドラッグのコールバック登録

        # 点と線を最上位に表示
        for selpoint in self.bb_points_info:
            self.cam_canvas.lift(selpoint["id_l"])
        for selpoint in self.bb_points_info:         # 点が上
            self.cam_canvas.lift(selpoint["id_c"])

    def __bb_circle_dragged(self, event):    # 点ドラッグ時コールバック
        # キャンバス内判定
        if event.x < 0:
            event.x = 0
        if event.y < 0:
            event.y = 0
        if event.x > self.CAM_CANVAS_X:  # カメラ画像サイズ
            event.x = self.CAM_CANVAS_X
        if event.y > self.CAM_CANVAS_Y:
            event.y = self.CAM_CANVAS_Y

        # イベント発生した場所に近いオブジェクトを取得
        #item_id = self.canvas.find_closest(event.x, event.y)    # 速くドラッグすると外れてしまう
        dist = 999999999
        itemidx = 0
        for idx, selitem in enumerate(self.bb_points_info):
            disttmp = (selitem["x"]-event.x) * (selitem["x"]-event.x) + (selitem["y"]-event.y) * (selitem["y"]-event.y)
            if dist > disttmp:
                itemidx = idx
                dist = disttmp

        # 点移動
        self.cam_canvas.move(self.bb_points_info[itemidx]["id_c"], event.x - self.bb_points_info[itemidx]["x"], event.y - self.bb_points_info[itemidx]["y"])
        self.bb_points_info[itemidx]["x"] = event.x
        self.bb_points_info[itemidx]["y"] = event.y

        # ライン移動
        if itemidx >= 3:
            nextidx = 0
        else:
            nextidx = itemidx+1
        if itemidx <= 0:
            lastidx = 3
        else:
            lastidx = itemidx-1
        self.cam_canvas.coords(self.bb_points_info[itemidx]["id_l"], event.x, event.y, self.bb_points_info[nextidx]["x"], self.bb_points_info[nextidx]["y"])
        self.cam_canvas.coords(self.bb_points_info[lastidx]["id_l"], self.bb_points_info[lastidx]["x"], self.bb_points_info[lastidx]["y"], event.x, event.y)

    def __button_open_raspi_camera_clicked(self):
        self.button_open_raspi_camera.config(state="disable")       # 処理終わるまで押せなくする
                                                                    # どうせgui更新されないから意味無し
        if self.cam_connected:  # カメラ接続済みなら
            self.cam_connected = False
            self.camera.closeCamera()
            self.button_open_raspi_camera.config(text=u"ラズパイカメラオープン", relief="raised")
            self.button_open_pc_camera.config(state="active")
        else:
            self.cam_connected = True
            self.camera = ET_Camera.ET_Camera(use_raspi_cam=True)
            self.button_open_raspi_camera.config(text=u"ラズパイカメラクローズ", relief="sunken")
            self.button_open_pc_camera.config(state="disable")

        self.button_open_raspi_camera.config(state="active")

    def __button_open_pc_camera_clicked(self):
        self.button_open_pc_camera.config(state="disable")       # 処理終わるまで押せなくする

        if self.cam_connected:  # カメラ接続済みなら
            self.cam_connected = False
            self.camera.closeCamera()
            self.button_open_pc_camera.config(text=u"PCカメラオープン", relief="raised")
            self.button_open_raspi_camera.config(state="active")
        else:
            self.cam_connected = True
            self.camera = ET_Camera.ET_Camera(use_raspi_cam=False,sel_pc_camera=0)
            self.button_open_pc_camera.config(text=u"PCカメラクローズ", relief="sunken")
            self.button_open_raspi_camera.config(state="disable")

        self.button_open_pc_camera.config(state="active")


    # 画面に認識結果を表示する
    def __setRecoResult(self, result):
        pass

    # 走行開始判定
    def __judgeStartGame(self):
        # BT接続済み
        # 走行体の状態が，ライントレース かつ 走行中 になった
        return False

    # 認識実行
    def __recognition(self, Course, Img, bb_area_points, number_card_points):

        # 数字部切り出し画像, ビンゴエリア切り出し画像 = 画像切り出し(Img, bb_area_points, number_card_points)

        # 数字認識(数字部切り出し画像)

        # 初期配置認識(Course, ビンゴエリア切り出し画像)

        # 戻り値つくる
        result = 0
        return result

    # 数字認識
    # 時間かかる可能性あり
    # def __数字認識(self, 切り出した画像):
    #  認識系要実装

    # 初期配置取得
    # 時間かかる可能性あり
    # def __初期配置取得(self, Course, 切り出した画像):
    #  認識系要実装




if __name__ == "__main__":
    gui = GUI()
    gui.root.mainloop()

