import sys
import tkinter as tk
import tkinter.messagebox as tkm


root = tk.Tk()
root.title(u"tkinter test")
root.geometry("1280x720")

# ボタンが押されたら呼び出される関数
def deleteEntry(event):
    # Entryの中身をすべて削除します
    Entry1.delete(0, tk.END)
    selidx = ListBox1.curselection()
    #ListBox1.selection_set(tk.END)      # リストの最後を選択
    ListBox1.selection_set(0)      # リストの最後を選択
    ListBox1.delete(selidx)
    print(Entry1.get())
    canvas.delete('rectangle1')

def showMessage(textstr):
    #tkm.showinfo('info', textstr)
    addList(Entry1.get())

def addList(text):
    ListBox1.insert(tk.END, text)


# テキスト
Static1 = tk.Label(text=u'text test', foreground='#ff0000', background='#00ff00')
Static1.pack()     # いい感じの位置に表示する
#Static1.place(x=150, y=200)

# テキストボックス
Entry1 = tk.Entry(width=100)
Entry1.insert(tk.END, u'デフォルト値')
#Entry1.place(x=200, y=200)
Entry1.pack()

print(Entry1.get())
#Entry1.delete(0, tk.END)   # 0文字目～最後を消す
#print(Entry1.get())

# Buttonを設置してみる
Button1 = tk.Button(text=u'消すボタン', width=50, height=2)
Button1.bind("<Button-1>", deleteEntry)        # ボタンが押されたときに実行される関数をバインドします
Button1.pack()

Button2 = tk.Button(text=u'リストに追加', command=lambda: showMessage(Entry1.get()))
Button2.pack()

# リストボックス
ListBox1 = tk.Listbox()
ListBox1.pack()


#キャンバスエリア
canvas = tk.Canvas(root, width = 400, height = 300)
canvas.place(x=10, y=100)          # キャンバスエリアを(0,0)で指定
canvas.create_rectangle(5, 5, 20, 20, fill='green', tag="rectangle1")         # 四角形で塗りつぶし
canvas.create_rectangle(5, 25, 20, 50, fill='red', tag="rectangle2")         # 四角形で塗りつぶし

# スケール,ラベルフレーム
labelframe = tk.LabelFrame(root, text="数字カード", width=150, height=180)
labelframe.propagate(False)
#labelframe.pack(side=tk.LEFT, fill="both", expand="yes")
labelframe.place(x=20, y=200)

scaleval = tk.IntVar()
scaleval.set(0)
sc = tk.Scale(labelframe, orient='h', from_=0, to=100, variable=scaleval, length=140)
sc.place(x=0, y=110)

root.mainloop()
