import sys
import gui.ET_gui as ET_gui


class Main:
    def __init__(self):
        self.gui = ET_gui.GUI()
        self.gui.root.mainloop()


if __name__ == "__main__":
    Main()

