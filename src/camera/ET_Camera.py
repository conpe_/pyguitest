import cv2
from threading import(Event, Thread)

class ET_Camera:
    def __init__(self, use_raspi_cam=True, sel_pc_camera=0):
        if use_raspi_cam:
            print("open RaspberryPi camera")
            self.openCamera("http://192.168.11.100/?action=stream")
        else:
            print("open this PC\'s camera")
            self.openCamera(sel_pc_camera)

    def openCamera(self, sel_cam):
        self.cap = cv2.VideoCapture(sel_cam)
        if self.cap.isOpened():
            #self.cap.set(cv2.CV_CAP_PROP_BUFFERSIZE,1)
            _,self.frame=self.cap.read()    # 初回読み込み
            self.frame = self.frame[:, :, ::-1]     # RGB

        # スレッド実行
        self.event = Event()
        t = Thread(target=self.updateFrame)
        t.start()
        print("start camera capture thread")

    def closeCamera(self):
        self.cap.release()
        self.event.set()    # スレッドを終わらせる(終わってくれないぽい)
        cv2.destroyAllWindows()

    def updateFrame(self):      # カメラ画像を更新する(別スレッド)
        while not self.event.wait(0.1):     # 更新周期
            if self.cap.isOpened():
                for i in range(3):      # 余分なフレームを捨てる   # この位の数字なら5分程度動かしても遅延は増えなかった
                    self.cap.grab()
                _, self.frame = self.cap.read()
                self.frame = self.frame[:, :, ::-1]     # RGB
        print("finish camera capture thread")


